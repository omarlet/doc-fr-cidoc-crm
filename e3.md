> # E3 Condition State
>
> | | |
> |---|---|
> | Subclass of: |   	E2 Temporal Entity
> | Scope note: |	This class comprises the states of objects characterised by a certain condition over a time-span.   An instance of this class describes the prevailing physical condition of any material object or feature during a specific instance of E52 Time Span. In general, the time-span for which a certain condition can be asserted may be shorter than the real time-span, for which this condition held.   The nature of that condition can be described using P2 has type. For example, the instance of E3 Condition State “condition of the SS Great Britain between 22 September 1846 and 27 August 1847” can be characterized as an instance “wrecked” of E55 Type. 
> | Examples: |  - the "reconstructed" state of the “Amber Room” in Tsarskoje Selo from summer 2003 until now (Owen, 2009)<br/>  - the "ruined" state of Peterhof Palace near Saint Petersburg from 1944 to 1946 (Maddox, 2015)<br/>  - the state of my turkey in the oven at 14:30 on 25 December, 2002 (P2 has type: E55 Type “still not cooked”)<br/>  - the topography of the leaves of Sinai Printed Book 3234.2361 on the 10th of July 2007 (described as: of type "cockled")
> | In First Order Logic: |   	E3(x) ⊃ E2(x)
> | Properties: |  P5 consists of (forms part of): E3 Condition State

# E3

| | |
|---|---|
| Sous-classe de |  |
| Super-classe de |  |
| Scope note: |  |
| Exemples: |  |
| In First Order Logic: |  |
| Propriétées: |  |
