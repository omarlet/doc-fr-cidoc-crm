# Doc fr CIDOC CRM

L'objectif est de traduire en français la documentation du CIDOC CRM la plus à jour. 

Nous partons de la version la plus récente, à savoir la V6.2.7, disponible malheureusement qu'au format docx:  

(http://www.cidoc-crm.org/versions-of-the-cidoc-crm). 

La version V7 devrait etre disponible a l'ete 2020.

En cas de problème, vous pouvez poser une question via un _Ticket_ (issue en anglais)

## Planning
* Mise en place: mi décembre 2019
* Invitations contributeurs: printemps 2020
* Début travail collaboratif: printemps 2020...

## Liste des contributeurs enregistrés:
| Prénom Nom | email | lien gitlab |
| ------ | ------ | ------ |
| Anais Guillem       | aguillem@ucmerced.edu; anais.guillem@gmail.com        | @ganais |
| Bulle Leonetti      | bulle.leonetti@inha.fr                                |
| Hélène Jamet        | helene.jamet@mom.fr                                   |
| Olivier Marlet      | olivier.marlet@univ-tours.fr                          | 
| Danielle Ziebelin   | Danielle.Ziebelin@imag.fr                             |
| Christophe Tuffery  | christophe.tuffery@inrap.fr                           |
| Raphaëlle Krummeich | raphaelle.krummeich@univ-rouen.fr                     | @rkrummeich |
| Diane Rego          | diane.rego@unicaen.fr                                 |
| Emmanuelle Morlock  | emmanuelle.morlock@mom.fr                             | @emorlock |
| Bertrand David      | bertrand.david@cnrs.fr                                | @bdavid |
| Adeline Levivier    | adeline.levivier@gmail.com; adeline.levivier@efeo.net |
| Juliette Hueber     | juliette.hueber@inha.fr                               |
| Aurelia Vasile      | aurelia.vasile@uca.fr                                 |
| Vincent Alamercy    | vincent.alamercery@ens-lyon.fr                        |
| Aurelia VASILE      | aurelia.vasile@uca.fr                                 |
| Romain Boissat      | romain.boissat@mom.fr                                 | @rboissat |
| Melanie Roche       | melanie.roche@bnf.fr                                  |
| George Bruseker     | george.bruseker@gmail.com                             |
